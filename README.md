# airflow-plugins

This repository contains airflow plugins to make Bizzflow nice and better.

## Plugin list

### Buttons

Adds buttons to the Airflow main menu.

- Bizztreat -> Storage Console
- Bizztreat -> Sandbox Console
- Bizztreat -> Cooltivator

## Installation

Clone this repo to `$AIRFLOW_HOME/plugins`, e.g.: /home/carl/airflow/plugins
