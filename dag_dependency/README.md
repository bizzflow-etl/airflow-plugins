# DAG dependency Airflow plugin

This adds ExternalDAGSensor. It allows you to check whether another DAG has run successfully in specified time frame and if not, start it.

## Usage

```python
from datetime import datetime, timedelta
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors import ExternalDAGSensor

dag = DAG(
    dag_id="test_dag_depndency",
    schedule_interval=None,
    start_date=datetime.now() - timedelta(7)
)

sensor = ExternalDAGSensor(
    dag=dag,
    external_dag_id="another_dag",
    data_age=timedelta(hours=6),
    task_id="require_another_dag",
    poke_interval=60,
    mode="poke" # or "reschedule" if you want to save worker threads
)

dummy_task = DummyOperator(dag=dag, task_id="second_task")

sensor >> dummy_task
```

Note that with the code above, the `second_task` won't run until `another_dag` has finished successfully. If `another_dag` finises with error, `test_dag_dependency` will also fail.
