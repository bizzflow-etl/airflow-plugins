import datetime
import os
import pendulum

from sqlalchemy import func

from airflow.plugins_manager import AirflowPlugin
from airflow.exceptions import AirflowException
from airflow.models import DagBag, DagModel, DagRun, TaskInstance, DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.base_sensor_operator import BaseSensorOperator
from airflow.utils.db import provide_session
from airflow.utils.decorators import apply_defaults
from airflow.utils.state import State

class ExternalDAGSensor(BaseSensorOperator):
    """
    Waits for a different DAG to complete for a specific end_date interval
    """

    @apply_defaults
    def __init__(self,
                 external_dag_id: str,
                 data_age: datetime.timedelta = None,
                 *args,
                 **kwargs):
        super(ExternalDAGSensor, self).__init__(*args, **kwargs)

        if data_age is None or not isinstance(data_age, datetime.timedelta):
            raise ValueError("data_age must be a valid datetime.timedelta object")

        self.data_age = data_age
        self.external_dag_id = external_dag_id

    @provide_session
    def poke(self, context, session=None):
        # self.log.info(
        #     'Poking for DAG %s to be run betwen %s and ... ',
        #     self.external_dag_id, serialized_dttm_filter
        # )
        start_date = pendulum.instance(context["ti"].start_date)
        now = pendulum.Pendulum.now()
        reference_date = start_date - self.data_age
        self.log.info("Poking for DAG %s to have been run between %s and %s", self.external_dag_id, reference_date, now)

        dagbag = DagBag()
        if not self.external_dag_id in dagbag.dags:
            raise ValueError("The specified DAG {} does not exist".format(self.external_dag_id))
        
        target_dag = dagbag.dags[self.external_dag_id]
        dag_runs = target_dag.get_dagruns_between(reference_date, now)

        if not dag_runs:
            self.log.info("Dag %s has not run in specified interval", self.external_dag_id)
            self.log.info("Scheduling dagrun for dag %s", self.external_dag_id)
            target_dag.create_dagrun(self._get_run_id(), State.NONE)
            return False
        
        dag_runs.sort(key=lambda d: d.end_date, reverse=True)

        latest_run = dag_runs[0]

        if latest_run.state == State.FAILED:
            self.log.warning("Last run of required DAG %s failed", self.external_dag_id)
            if not latest_run.external_trigger:
                raise AirflowException("Last run of required DAG {} failed".format(self.external_dag_id))
            self.log.info("The failed run was a manual trigger, we will attempt to rerun.")
            self.log.info("Scheduling dagrun for dag %s", self.external_dag_id)
            target_dag.create_dagrun(self._get_run_id(), State.NONE)
        elif latest_run.state == State.RUNNING:
            self.log.info("Latest run of %s is still running", self.external_dag_id)
            return False
        elif latest_run.state == State.SUCCESS:
            self.log.info("Requirement fullfilled")
            return True
    
    def _get_run_id(self):
        return "required_{}".format(datetime.datetime.now().strftime("%Y%m%d%H%M%S%f"))

class DAGDependencyPlugin(AirflowPlugin):
    name = "DAGDependencyPlugin"
    sensors = [ExternalDAGSensor]
    hooks = []
    executors = []
    macros = []
    admin_views = []
    flask_blueprints = []
    menu_links = []
