# This belongs to AIRFLOW_HOME/plugins/buttons/__init__.py

"""Bizztreat Bizzflow plugin for Airflow
This adds /storage and /sandbox buttons to Airflow UI.
"""

from airflow.plugins_manager import AirflowPlugin
from flask_admin.base import MenuLink

flow_ui = MenuLink(
    category="Consoles",
    name="Flow UI",
    url="/flow/"
)

appbuilder_flow_ui = {
    "name": "Flow UI",
    "category": "Consoles",
    "category_icon": "fa-terminal",
    "href": "/flow/"
}

storage = MenuLink(
    category="Consoles",
    name="Storage Console",
    url="/storage/"
)

appbuilder_storage = {
    "name": "Storage Console",
    "category": "Consoles",
    "category_icon": "fa-terminal",
    "href": "/storage/"
}

cooltivator = MenuLink(
    category="Consoles",
    name="Cooltivator",
    url="https://cooltivator.appspot.com/",
    target="_blank"
)

appbuilder_cooltivator = {
    "name": "Cooltivator",
    "category": "Consoles",
    "category_icon": "fa-terminal",
    "href": "https://cooltivator.appspot.com/",
}

latest_tasks = MenuLink(
    category="Consoles",
    name="Latest Tasks",
    url="/admin/taskinstance/?sort=3&desc=1"
)


class BizztreatLinksPlugin(AirflowPlugin):
    name = "BizztreatLinksPlugin"
    operators = []
    flask_blueprints = []
    hooks = []
    executors = []
    admin_views = []
    menu_links = [flow_ui, storage, cooltivator, latest_tasks]
    appbuilder_menu_items = [appbuilder_flow_ui, appbuilder_storage, appbuilder_cooltivator]
